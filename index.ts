import 'reflect-metadata';
import { Container, injectable } from 'inversify';
import { DIBuilder } from './di-builder';
import { ATest, Test1, Test2 } from './test-classes';

const c = new Container({
    autoBindInjectable: true,
    skipBaseClassChecks: true,
});

c.bind(ATest).to(Test1);

c.bind(Test2).toDynamicValue((c) => {
    const t = new Test1();
    t.mutableArg = 2;
    const ctrArgs: any[] = Reflect.getMetadata('design:paramtypes', Test2);

    const index = ctrArgs.findIndex((a) => typeof a == typeof Test1);
    const params: ConstructorParameters<typeof Test2> = ctrArgs.map((a) =>
        c.container.get(a)
    ) as any;
    params[index] = t;
    return new Test2(...params);
});

// c.rebind(ATest).to(Test2);

console.log(c.get(Test2).test());
