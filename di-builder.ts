import { Container, interfaces } from 'inversify';

declare type Constructor<T> = Function & { prototype: T }; // new (...args: any[]) => T;

export class DIBuilder<TBase> {
    private _base: Constructor<TBase>;
    private _to: any;
    private _toDynamic: any;

    private _partialParamType: any;
    private _partialBuilder: any;

    constructor(private _container: Container) {}

    static build<T>(c: Container, arg: Constructor<T>): DIBuilder<T> {
        const b = new DIBuilder(c);
        b._base = arg;

        return b;
    }

    // todo: arg must be typeof T
    to(arg: any): DIBuilder<TBase> {
        this._to = arg;
        return this;
    }

    // todo: ensure this blocks non T results
    toDynamic(arg0: (c: Container) => TBase): DIBuilder<TBase> {
        this._toDynamic = arg0;
        return this;
    }

    // todo: add/simplify ability to perform multiple overrides for the same parameter
    // todo: refactor to add ability to override multiple parameters
    withPartialOverride<TOverride>(
        paramToOverride: Constructor<TOverride>,
        builder: (c: Container) => TOverride
    ) {
        this._partialParamType = paramToOverride;
        this._partialBuilder = builder;
        return this;
    }

    bind(): void {
        if (this._to == null && this._toDynamic == null) {
            throw new Error('No bind target defined');
        }

        const s1 = this.performBind();
        this.performTo(s1);
        // todo: scope
    }

    private performBind() {
        return this._container.bind(this._base);
    }

    private performTo(s1: interfaces.BindingToSyntax<any>) {
        if (this._partialParamType != null && this._to != null) {
            return s1.toDynamicValue((c) => {
                const overridenParam = this._partialBuilder(c.container);
                const ctrArgs: any[] = Reflect.getMetadata(
                    'design:paramtypes',
                    this._to
                );

                const index = ctrArgs.findIndex(
                    (a) => typeof a == typeof this._partialParamType
                );
                const params: ConstructorParameters<typeof this._to> =
                    ctrArgs.map((a) => c.container.get(a)) as any;
                params[index] = overridenParam;
                return new this._to(...params);
            });
        }

        if (this._toDynamic != null) {
            return s1.toDynamicValue((c) => this._toDynamic(c.container));
        }

        if (this._to != null) {
            return s1.to(this._to);
        }

        throw new Error('Unable to complete "to" syntax');
    }

    // todo performScope
}
