import { injectable } from "inversify";

@injectable()
export abstract class ATest {
    constructor() {}

    abstract test(): string;
}

@injectable()
export class Test1 extends ATest {
    public mutableArg = 1;
    constructor() {
        super();
    }

    test() {
        return 'test' + this.mutableArg.toString();
    }
}

@injectable()
export class OptionalTest1 {
    constructor() {}

    optionalTest() {
        return 'optional test';
    }
}

@injectable()
export class Test2 {
    constructor(private baseTest: ATest, private optionalTest: OptionalTest1) {}

    test() {
        return (
            'extended ' +
            this.baseTest.test() +
            ' ' +
            this.optionalTest.optionalTest()
        );
    }
}