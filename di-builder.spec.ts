import 'reflect-metadata';
import { Container } from 'inversify';
import { DIBuilder } from './di-builder';
import { ATest, Test1, Test2 } from './test-classes';

describe('DIBuilder tests', () => {
    test('inversify should work', () => {
        const c = new Container();
        c.bind(ATest).to(Test1);
        expect(c.get(ATest).test()).toBe('test1');
    });

    test('should bind classes', () => {
        const c = new Container();
        DIBuilder.build(c, ATest).to(Test1).bind();
        expect(c.get(ATest).test()).toBe('test1');
    });

    test('should bind dynamic values', () => {
        const c = new Container({
            autoBindInjectable: false,
            skipBaseClassChecks: true,
        });
        DIBuilder.build(c, ATest)
            .toDynamic((_) => {
                return new Test1();
            })
            .bind();
        expect(c.get(ATest).test()).toBe('test1');
    });

    test('should bind with partial overriding', () => {
        const c = new Container({
            autoBindInjectable: true,
        });
        DIBuilder.build(c, Test2)
            .to(Test2)
            .withPartialOverride(Test1, (c) => {
                const t1 = c.get(Test1);
                t1.mutableArg = 2;
                return t1;
            })
            .bind();
        expect(c.get(Test2).test()).toBe('extended test2 optional test');
    });
});
